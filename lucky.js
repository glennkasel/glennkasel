var newGame = 0; // global game count

function startGame(){
var die1, die2 = 0;
var loop = 0; //loop counter
var current = new Array();
var start = parseInt(document.getElementById("bet").value)
console.log("hello, the starting amount is "+start);//used to see what was happening in the console

	if (isNaN (start) || start < 1){
		return alert("Ooops. Invalid Input. Please enter a positive integer and click Play");
		}
	else{ 
		while (start>0){
		current.push(start);	
		die1 = (Math.floor(Math.random() * 6) + 1);
		die2 = (Math.floor(Math.random() * 6) + 1);
		var tot = (die1+die2);
		console.log("the total on the dice is " + tot); //used to see what was happening in the console
		
		if (tot == 7){
			start += 4;
		} else {
			start -= 1;
		}
		console.log(current[loop]); // used to track in the console
		loop++;
		}
		console.log("there were "+ loop +" rolls before you went broke");  //used to track in the console
		var high = Math.max(...current);
		var roll = current.indexOf(Math.max(...current)) + 1; //added 1 as indexes start at zero and call it rolls
		console.log("highest amount during play was $"+ high); // still tracking
		}
		
		if (newGame>=1){ //an if statement to remove the 1st games data
			a = document.getElementById("row1"); // these variables were already declared in the first game
			a.innerHTML = '';	
			c = document.getElementById("row2");
			c.innerHTML = '';
			e = document.getElementById("row3");
			e.innerHTML = '';
			g = document.getElementById("row4");
			g.innerHTML = '';
			}
		
		var a = document.createElement("TD");
		var b = document.createTextNode("$ "+current[0]+".00");
		a.appendChild(b);
		document.getElementById("row1").appendChild(a);
		
		var c = document.createElement("TD");
		var d = document.createTextNode(loop);
		c.appendChild(d);
		document.getElementById("row2").appendChild(c);
		
		var e = document.createElement("TD");
		var f = document.createTextNode("$ " + high +".00");
		e.appendChild(f);
		document.getElementById("row3").appendChild(e);
		
		var g = document.createElement("TD");
		var h = document.createTextNode(roll);
		g.appendChild(h);
		document.getElementById("row4").appendChild(g);
		
		if (newGame ==0){
			document.getElementById("replay").innerHTML = "Play Again"; // changes button to play again after 1st game starts 
			}
		newGame++; // for global game count
		if (newGame>=5) {
			return alert("Wow, You played this game "+newGame+ " times!! You can keep playing.")
			}
	}